const envoye = document.querySelector('.envoye');
const categorieList = document.querySelector('#recette');
const categorie = document.querySelector('.categorieRecette');

categorieList.addEventListener('categorie selectione', afficherRecettes);

envoye.addEventListener('click', rechercheRecette);
function afficherRecettes(e) {
    e.preventDefault();



  const categorieSelectione = e.target.value;
  


  fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${categorieSelectione}`)
    .then(response => response.json())
    .then(data => {
      let contentHtml = "";
      data.meals.forEach(meal => {
       
        html += `
          <div class="recette">
            <img src="${meal.strMealThumb}">
            <h3>${meal.strMeal}</h3>
          </div>
        `;
      });
      
      categorieList.innerHTML = contentHtml;
    });
}

function rechercheRecette(e) {
  e.preventDefault();



  let inputTxt = document.getElementById('input').value.trim();

  fetch(`https://www.themealdb.com/api/json/v1/1/list.php?c=${inputTxt}`)
    .then(response => response.json())
    .then(data => {

        
      let contentHtml = "";
      if (data.meals) {
        contentHtml += `<select id="categorieRecette">`;
          data.meals.forEach(meal => {
            contentHtml += `<option value="${meal.strCategory}">
          ${meal.strCategory}
          </option>`
        })
        contentHtml += `</select>`;
      } 

      categorie.innerHTML = contentHtml;
    })
}

