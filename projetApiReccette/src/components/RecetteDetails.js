
export class RecetteDetails {
    _meals;
    _description;
    constructor(meals, description) {
      this._meals = meals;
      this._description = description;

    }
  
    get content() {
        const li = document.createElement("li")
        li.classList.add("carte")
        const contenuHtml =
        `
        <div style="display: inline-block">
            <img src="${this._meals[0].strMealThumb}" class="recetteimg">
            <h2 class="nomrecette" >${this._meals[0].strMeal}</h2>
            <button type="text" class="recette ">${this._description[0].strInstructions}Recette</button>
        </div>
        `
        li.innerHTML = contenuHtml
        return li
        
    } 
}
