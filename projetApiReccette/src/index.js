const envoye = document.querySelector('.envoye');
const aleatoire = document.querySelector('.aleatoire');
const categorie = document.querySelector('.categorie');


const recette = document.querySelector('#recette');  

const popupBtn = document.querySelector('.popupBtn');
const popup = document.querySelector('.popup');
const fermer = document.querySelector('.fermer');

const apiKey = "";


envoye.addEventListener('click', rechercheRecette);
aleatoire.addEventListener('click', rechercheAleatoire);
// popup.addEventListener('click', popUp);


function rechercheAleatoire (){
   
fetch("https://www.themealdb.com/api/json/v1/1/random.php")

.then(response => response.json())
    .then(data => {
        console.log(data);
        let html = "";
        if (data.meals) {
            data.meals.forEach(meal => {
                html += `
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                </div>

                `
            })
        }  
            recette.innerHTML = html  
        })
        
}for (let i = 0; i < 6; i++) {
    rechercheAleatoire();
}
    
    

function rechercheRecette(e){    
    e.preventDefault();
    // const API_URL = "https://www.themealdb.com/api/json/v1/1/";
    // const SEARCH_BY_NAME = "search.php?";
    let inputTxt = document.getElementById('input').value.trim();

    fetch(`https://www.themealdb.com/api/json/v1/1/search.php?s=${inputTxt}`)
    // fetch(
    //     `${API_URL + SEARCH_BY_NAME}` +
    //     new URLSearchParams({
    //         s: inputTxt,
    //     }),
    //     {
    //         method: "GET",
    //     }
    // )
    .then(response => response.json())
    .then(data => {
        console.log(data);
        let html = "";
        if (data.meals) {
            data.meals.forEach(meal => {
                html += `
                <div class="carte" data-id = "${meal.idMeal}">
                    <img src="${meal.strMealThumb}" class="recetteimg">
                    <h3 class="nomrecette" >${meal.strMeal}</h3>
                    <a href="#" class="description">Recette</a>
                </div>
                `
            })
        } else {
            html = `Recette non trouvable`
        }

        recette.innerHTML = html;
    })
}
